import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int counter = 0;
  List listDemo = List.empty(growable: true);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 2, 167, 177),
        title: Text(
          "To-do list",
          style: GoogleFonts.quicksand(
            fontSize: 26,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: listDemo.length,
        itemBuilder: (context, index) {
          return Container(
            margin: const EdgeInsets.only(top: 20, left: 15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: const Color.fromARGB(255, 250, 232, 232),
            ),
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      height: 75,
                      margin:
                          const EdgeInsets.only(top: 30, left: 12, right: 90),
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color.fromARGB(255, 255, 255, 255),
                      ),
                      child: Image.asset(
                        "assets/images/img1.png",
                      ),
                    ),
                    Column(
                      children: [
                        SizedBox(
                          width: 50,
                          height: 50,
                          child: Text(
                            "Lorem Ipsum is simply setting industry.",
                            style: GoogleFonts.quicksand(
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                              color: const Color.fromARGB(1, 0, 0, 0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 30,
                    ),
                    Text(
                      "10 July 2024",
                      style: GoogleFonts.quicksand(
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                        color: const Color.fromARGB(255, 132, 132, 132),
                      ),
                    ),
                    const Spacer(),
                    const Icon(
                      Icons.edit,
                      color: Color.fromARGB(255, 0, 139, 148),
                    ),
                    const Icon(
                      Icons.delete_outlined,
                      color: Color.fromARGB(255, 0, 139, 148),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            counter++;
            listDemo.add(counter);
          });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
