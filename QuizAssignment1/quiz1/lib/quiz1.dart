import 'package:flutter/material.dart';

class QuizApp1 extends StatefulWidget{
  const QuizApp1({super.key});

  @override
  State<QuizApp1> createState()=> _QuizApp1State();
}

class _QuizApp1State extends State<QuizApp1>{

  List<Map> allQuestions = [
    {
      "question": "Who is the founder of Microsoft?",
      "options":["Steve Jobs","Jeff Bezos","Bill Gates","Elon Musk"],
      "answerIndex":2,
    },

    {
      "question": "Who is the founder of Apple?",
      "options":["Steve Jobs","Bill Gates","Elon Musk","Mark Zuckerrburg"],
      "answerIndex":0,
    },
    {
      "question":"Who is the founder of Google?",
      "options":["Lary Page","Bill Gates","Elon Musk","Steve Jobs"],
      "answerIndex":0,
    },
    {
      "question":"Who is the founder of Google?",
      "options":["Lary Page","Bill Gates","Elon Musk","Steve Jobs"],
      "answerIndex":0,
    },
    {
      "question":"Who is the founder of Google?",
      "options":["Lary Page","Bill Gates","Elon Musk","Steve Jobs"],
      "answerIndex":0,
    },
    {
      "question":"Who is the founder of Google?",
      "options":["Lary Page","Bill Gates","Elon Musk","Steve Jobs"],
      "answerIndex":0,
    },
    {
      "question":"Who is the founder of Google?",
      "options":["Lary Page","Bill Gates","Elon Musk","Steve Jobs"],
      "answerIndex":0,
    },
    {
      "question":"Who is the founder of Google?",
      "options":["Lary Page","Bill Gates","Elon Musk","Steve Jobs"],
      "answerIndex":0,
    },
    {
      "question":"Who is the founder of Google?",
      "options":["Lary Page","Bill Gates","Elon Musk","Steve Jobs"],
      "answerIndex":0,
    },
    {
      "question":"Who is the founder of Google?",
      "options":["Lary Page","Bill Gates","Elon Musk","Steve Jobs"],
      "answerIndex":0,
    },
    
  ];

  bool questionScreen = true;
  int questionIndex = 0;

  Scaffold isQuestionScreen(){
    if(questionScreen){
      return Scaffold(
        appBar:AppBar(
          title: const Text(
            "QuizApp",
            style:TextStyle(
              fontSize:30,
              fontWeight:FontWeight.w800,
              color:Colors.orange,
            ),
          ),

          centerTitle:true,
          backgroundColor:Colors.blue,
        ),

        body:Column(
          children:[
            const SizedBox(
              height:25,
            ),

            Row(
              mainAxisAlignment:MainAxisAlignment.center,
              children:[
                const Text(
                  "Questions: ",
                  style:TextStyle(
                    fontSize:25,
                    fontWeight:FontWeight.w600,
                  ),
                ),

                Text(
                  "${questionIndex+1}/${allQuestions.length}",
                  style:const TextStyle(
                    fontSize:25,
                    fontWeight:FontWeight.w600,
                  ),
                ),
              ],
            ),

            const SizedBox(
              height:50,
            ),

            SizedBox(
              width:380,
              height:50,
              child:Text(
                allQuestions[questionIndex]["question"],
                style:const TextStyle(
                  fontSize:23,
                  fontWeight:FontWeight.w400,
                ),
              ),
            ),

            const SizedBox(
              height:30,
            ),


            ElevatedButton(
              onPressed:(){},
              child:Text(
                "A.${allQuestions[questionIndex]["options"][0]}",
                style:const TextStyle(
                  fontSize:20,
                  fontWeight:FontWeight.normal,
                ),
              ),
            ),

            ElevatedButton(
              onPressed:(){},
              child:Text(
                "B.${allQuestions[questionIndex]["options"][1]}",
                style:const TextStyle(
                  fontSize:20,
                  fontWeight:FontWeight.normal,
                ),
              ),
            ),

            ElevatedButton(
              onPressed:(){},
              child:Text(
                "C.${allQuestions[questionIndex]["options"][2]}",
                style:const TextStyle(
                  fontSize:20,
                  fontWeight:FontWeight.normal,
                ),
              ),
            ),

            ElevatedButton(
              onPressed:(){},
              child:Text(
                "D.${allQuestions[questionIndex]["options"][3]}",
                style:const TextStyle(
                  fontSize:20,
                  fontWeight:FontWeight.normal,
                ),
              ),
            ),
          ],
        ),

        floatingActionButton:FloatingActionButton(
          onPressed:(){},
          backgroundColor:Colors.blue,
          child:const Icon(
            Icons.forward,
            color:Colors.orange,
          ),
        ),
      );

    }else{
      return const Scaffold();
    }
  }

  @override
  Widget build(BuildContext context){
    return isQuestionScreen();
  }
}