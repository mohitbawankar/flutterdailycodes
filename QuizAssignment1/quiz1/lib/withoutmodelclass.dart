
import 'package:flutter/material.dart';

class QuizApp extends StatefulWidget{
  const QuizApp({super.key});

  @override
  State<QuizApp> createState()=> _QuizAppState();
}

class _QuizAppState extends State<QuizApp>{

  List<Map> allQuestions = [
    {
      "question":"Who is the founder of Apple?",
      "options":["Steve Jobs","Bill Gates","Elon Musk","Ratan Tata"],
      "answerIndex":0,
    },

    {
      "question":"Who is the founder of Microsoft?",
      "options":["Steve Jobs","Bill Gates","Elon Musk","Ratan Tata"],
      "answerIndex":1,
    },

    {
      "question":"Who is the founder of SpaceX?",
      "options":["Steve Jobs","Bill Gates","Elon Musk","Ratan Tata"],
      "answerIndex":2,
    },

    {
      "question":"Who is the founder of TCS?",
      "options":["Steve Jobs","Bill Gates","Elon Musk","Ratan Tata"],
      "answerIndex":3,
    },

    {
      "question":"Who is the founder of Google?",
      "options":["Steve Jobs","Bill Gates","Elon Musk","Lary Page"],
      "answerIndex":3,
    },

    {
      "question":"Who is the founder of Meta?",
      "options":["Steve Jobs","Bill Gates","Mark Zuckerrburg","Ratan Tata"],
      "answerIndex":2,
    },

    {
      "question":"Who is the founder of Amazon?",
      "options":["Jeff Bezos","Bill Gates","Elon Musk","Ratan Tata"],
      "answerIndex":0,
    },

    {
      "question":"Who is the founder of Adobe?",
      "options":["Steve Jobs","John Warnock","Elon Musk","Ratan Tata"],
      "answerIndex":1,
    },

    {
      "question":"Who is the founder of Git?",
      "options":["Linus Torvald","Bill Gates","Elon Musk","Ratan Tata"],
      "answerIndex":0,
    },

    {
      "question":"Who is the founder of Boat?",
      "options":["Steve Jobs","Bill Gates","Elon Musk","Aman Gupta"],
      "answerIndex":3,
    },
  ];

  bool questionScreen = true;
  int questionIndex = 0;
  int selectedAnswerIndex = -1;
  int noOfCorrectAnswers = 0;

  MaterialStateProperty<Color> checkAnswer(int buttonIndex){
    if(selectedAnswerIndex != -1){
      if(buttonIndex == allQuestions[questionIndex]["answerIndex"]){
        return const MaterialStatePropertyAll(Colors.green);
      }else if(buttonIndex == selectedAnswerIndex){
        return const MaterialStatePropertyAll(Colors.red);
      }else{
        return const MaterialStatePropertyAll(null);
      }
    }else{
      return const MaterialStatePropertyAll(null);
    }
  }


  void validateCurrentPage(){
    if(selectedAnswerIndex == -1){
      return ;
    }

    if(selectedAnswerIndex == allQuestions[questionIndex]["answerIndex"]){
      noOfCorrectAnswers = noOfCorrectAnswers + 1;
    }

    if(selectedAnswerIndex != -1){
      if(questionIndex == allQuestions.length-1){
        setState((){
          questionScreen = false;
        });
      }
      selectedAnswerIndex = -1;
      setState((){
        questionIndex +=1;
      });
    }
  }

  Scaffold isQuestionScreen(){
    if(questionScreen == true){
      return Scaffold(
        appBar:AppBar(
          title:const Text(
            "QuizApp ",
            style:TextStyle(
              fontSize:30,
              fontWeight:FontWeight.w800,
              color:Colors.orange,
            ),
          ),

          backgroundColor:Colors.blue,
          centerTitle:true,
        ),

        body:Column(
          children:[
            const SizedBox(
              height:25,
            ),
            Row(
              mainAxisAlignment:MainAxisAlignment.center,
              children:[
                const Text(
                  "Question: ",
                  style:TextStyle(
                    fontSize:25,
                    fontWeight:FontWeight.w600,
                  ),
                ),

                Text(
                  "${questionIndex+1}/${allQuestions.length}",
                  style:const TextStyle(
                    fontSize:25,
                    fontWeight:FontWeight.w600,
                  ),
                ),
              ],
            ),

            const SizedBox(
              height:50,
            ),

            SizedBox(
              width:380,
              height:50,
              child:Text(
                allQuestions[questionIndex]["question"],
                style:const TextStyle(
                  fontSize:23,
                  fontWeight:FontWeight.w400,
                ),
              ),
            ),

            const SizedBox(
              height:30,
            ),

            ElevatedButton(
              style:ButtonStyle(
                backgroundColor:checkAnswer(0),
              ),

              onPressed:()
            ),
          ],
        ),
      );
    }
  }
}