import 'package:flutter/material.dart';


class FlagCounter extends StatefulWidget{
  const FlagCounter({super.key});

  @override

  State<FlagCounter> createState()=> _FlagCounterState();
}

class _FlagCounterState extends State<FlagCounter>{

  int _count = 0;
  void incrementCounter(){
    setState((){
      _count = _count + 1;
    });
  }

  @override 

  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        title:const Text("Flag"),
      ),

      body:Center(

        child:Column(

          mainAxisAlignment:MainAxisAlignment.center,

          children:[

            const Text(
              "Click here to increase counter",
            ),

            Text(
              "$_count",
            ),
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed:incrementCounter,
        child:const Icon(Icons.add),
      ),
    );
  }
}