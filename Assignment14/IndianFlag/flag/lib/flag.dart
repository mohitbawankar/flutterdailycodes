import 'package:flutter/material.dart';

class IndianFlag extends StatefulWidget{
  const IndianFlag({super.key});

  @override 
  State<IndianFlag> createState()=> _IndianFlagState();
}

class _IndianFlagState extends State<IndianFlag>{

  int _count = -1;
  @override

  Widget build(BuildContext context){

      return Scaffold(
        appBar:AppBar(
          title:const Text("Indian Flag"),
        ),
        floatingActionButton:FloatingActionButton(
          onPressed:(){
            setState((){
                _count = _count + 1;
            });
          },

          child:const Text("Add"),
        ),


        body:Container(
          color:Colors.grey[300],

          child: Row(
            mainAxisAlignment:MainAxisAlignment.center,
            crossAxisAlignment:CrossAxisAlignment.start,

            children:[
              (_count >= 0)
              ? Container(
                height:500,
                width:10,
                color:Colors.brown,
              )
              :Container(),

              Column(
                children:[
                  (_count >= 1)
                  ? Container(
                    height:80,
                    width:250,
                    color:Colors.orange,
                  )
                  :Container(),

                  (_count >= 2)
                  ? Container(
                    height:80,
                    width:250,
                    color:Colors.white,
                    child:(_count >= 3)
                      ? Image.network("https://photomedia.in/wp-content/uploads/2023/07/ashok-chakra-vector-1024x1024.png")
                      : Container(),
                  )
                  :Container(),

                (_count >= 5)
                ? Container(
                  height:80,
                  width:250,
                  color:Colors.green,
                )
                : Container(),
                ],
              ),
            ],
          ),
        ),

      );
  }
}