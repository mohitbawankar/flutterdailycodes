import 'package:flutter/material.dart';

class BottomSheetAssignment extends StatefulWidget {
  const BottomSheetAssignment({super.key});

  @override
  State<BottomSheetAssignment> createState() => _BottomSheetAssignmentState();
}

class _BottomSheetAssignmentState extends State<BottomSheetAssignment> {
  TextEditingController nameController = TextEditingController();
  String? name;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Bottom Sheet"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  decoration:BoxDecoration(
                    borderRadius: BorderRadius.all(
                  ),
                  color: const Color.fromRGBO(250, 232, 232, 1),
                  child: Column(
                    children: [
                      const Text("Create list"),
                      // const SizedBox(height: 5),
                      Column(
                        // width: 300,
                        // height: 150,
                        children: [
                          TextField(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                hintText: "Enter title"),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextField(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                hintText: "Enter Description"),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          TextField(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                hintText: "Enter Date"),
                          ),
                        ],
                      ),

                      ElevatedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text("Submit"))
                    ],
                  ),
                );
              });
        },
      ),
    );
  }
}
