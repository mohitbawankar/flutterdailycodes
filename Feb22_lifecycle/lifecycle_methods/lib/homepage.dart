import 'package:flutter/material.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State<Homepage> createState() {
    print("In createstate"); // createState()
    return _HomepageState();
  }
}

class _HomepageState extends State<Homepage> {
  int count = 0;

  bool isSame = false;
  @override
  void initState() {
    super.initState();
    print("In initState method");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("In didChangeDependencies");
  }

  @override
  void deactivate() {
    super.deactivate();
    print("In deactivated");
  }

  @override
  Widget build(BuildContext context) {
    print("In build method");

    if (isSame = false) {
      return Scaffold(
        appBar: AppBar(
          title: const Text("Lifecycle"),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState() {
              count++;
              isSame = true;
              print("State state");
            }

            child:
            Icon(Icons.add);
          },
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text("New Screen"),
        ),
        body: const Text("New Screen"),
      );
    }
  }
}
