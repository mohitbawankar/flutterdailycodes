
import 'package:flutter/material.dart';
import 'package:appusingallwidget/next.dart';


class App extends StatefulWidget{
  const App({super.key});

  @override
  State<App> createState()=> _AppState();

}


class _AppState extends State<App>{

  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor:Colors.white,
      appBar:AppBar(
        leading:const Icon(Icons.menu),
        title:const Text("Telegram",
          style:TextStyle(
            fontSize:20,
            fontStyle:FontStyle.italic,
          ),
        ),
        actions:const [
          Icon(Icons.search),

          SizedBox(
            width:10,
          ),
        ],

      ),

      body: SingleChildScrollView(
        
        child: Column(
          children:[
            Row(
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "Good Morning!!!"
                          ),
                          
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),
            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),

            Row(
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Abhishek",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "Notes send kar!!"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),

            Row(
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Prathamesh",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "Sunday la sagla explain kar!!!!"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Abhay",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "5 mintat class sathi nigh !!!"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Vaibhav",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "Reading hall madhna kadhi yenar ??"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Vishal",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "Python cha decorator explain kar aaj !!!!"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Pranav",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "DSA che code explain kar !!!"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),

            Container(
              width:386,
              height:1,
              color:Colors.black38,
            ),


            Row(
              
              children:[
                
                const Icon(Icons.supervised_user_circle_sharp,
                          size:70),
                Container(
                  width:300,
                  height:50,
                  child: const Column(
                    children: [
                  
                      Row(
                        children:[
                          Text("Aaditya",
                          style:TextStyle(
                            fontSize:15,
                            fontStyle: FontStyle.normal,
                          ),),
                          Spacer(),
                          Text("7/2/24"),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "ldnmkvnkl evjnvlev vlevlknlskv leklnf"
                          ),
                        ],
                      ),
                      
                    ],
                  ),
                ),
              ],
      
            ),
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed:() {
          setState((){
              const Next();
          });
        },
        child:const Icon(Icons.add),
      ),
    );
  }
}