
import 'package:flutter/material.dart';

class ColumnFormat extends StatelessWidget{
  const ColumnFormat({super.key});

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar:AppBar(
        title:const Text(
          "Column",
        ),
      ),

      body:Container(
        
        width:double.infinity,
        child:Column(
          
          mainAxisAlignment:MainAxisAlignment.center,
          crossAxisAlignment:CrossAxisAlignment.center,
          children:[
            Container(
              height:100,
              width:100,
              color:Colors.red,
            ),

            Container(
              height:100,
              width:100,
              color:Colors.green,
            ),

            Container(
              height:100,
              width:100,
              color:Colors.blue,
            ),
          ],
        ),
      ),
    );
  }
}