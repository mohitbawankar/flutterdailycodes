// Using model class

import "package:flutter/material.dart";


class Module {
  final int? answerIndex;
  final String? questions;
  final List<String>? options;
  const Module({this.answerIndex, this.options, this.questions});
}

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State createState() {
    return _QuizApp();
  }
}


class _QuizApp extends State {
  List allList = [
    const Module(        // Object creation of Module class
      questions: "Who developed the programming language 'C' ?",
      options: [
        "Dennis Ritchie",
        "Linus Torvalds",
        "Bill Joy",
        "Guido van Rossum",
      ],
      answerIndex: 0,
    ),
    const Module(
      questions: "Who is the inventor of linux?",
      options: [
        "Dennis Ritchie",
        "Linus Torvalds",
        "Bill Joy",
        "Douglas Engelbart",
      ],
      answerIndex: 1,
    ),
    const Module(
      questions: "Who is the inventor of mouse?",
      options: [
        "Dennis Ritchie",
        "Linus Torvalds",
        "Christopher Latham Sholes",
        "Douglas Engelbart",
      ],
      answerIndex: 3,
    ),
    const Module(
      questions: "Who is the inventor of keyboard?",
      options: [
        "Dennis Ritchie",
        "Linus Torvalds",
        "Christopher Latham Sholes",
        "Douglas Engelbart",
      ],
      answerIndex: 2,
    ),

    const Module(
      questions: "Who is the mind behind the creation of JavaScript?",
      options: [
        "Anders Hejlsberg",
        "Brendan Eich",
        "Douglas Crockford",
        "Ryan Dahl",
      ],
      answerIndex: 1,
    ),

    const Module(
      questions: "Who are the creators of the R programming language ?",
      options: [
        "Ross Ihaka and Robert Gentleman",
        "John McCarthy and Alan Perlis",
        "Guido van Rossum and Tim Peters",
        "Larry Page and Sergey Brin",
      ],
      answerIndex: 0,
    ),

    const Module(
      questions: "Who developed the C++ programming language?",
      options: [
        "Dennis Ritchie",
        "Linus Torvalds",
        "Bjarne Stroustrup",
        "Douglas Engelbart",
      ],
      answerIndex: 2,
    ),

    const Module(
      questions: "Who developed the python programming language?",
      options: [
        "Dennis Ritchie",
        "Linus Torvalds",
        "Christopher Latham Sholes",
        "Guido van Rossum",
      ],
      answerIndex: 3,
    ),

    const Module(
      questions: "Who developed the java programming language?",
      options: [
        "Dennis Ritchie",
        "Linus Torvalds",
        "James Gosling",
        "Douglas Engelbart",
      ],
      answerIndex: 2,
    ),

    const Module(
      questions: "Who developed the dart programming language?",
      options: [
        "Dennis Ritchie",
        "Linus Torvalds",
        "James Gosling",
        "Lars Bak and Kasper Lund",
      ],
      answerIndex: 3,
    ),
  ];
  bool isQuestionScreen = true;
  int questionIndex = 0;
  int selectedIndex = -1;
  int score = 0;
  MaterialStateProperty<Color?> checkAnswer(int buttonIndex) {
    if (selectedIndex != -1) {
      if (buttonIndex == allList[questionIndex].answerIndex) {
        if (buttonIndex == selectedIndex) {
          score++;
        }
        return const MaterialStatePropertyAll(Colors.green);
      } else if (buttonIndex == selectedIndex) {
        return const MaterialStatePropertyAll(Colors.red);
      } else {
        return const MaterialStatePropertyAll(Colors.white);
      }
    } else {
      return const MaterialStatePropertyAll(Colors.white);
    }
  }

  void pageNavigation() {
    if (selectedIndex != -1) {
      selectedIndex = -1;
      questionIndex++;
      if (questionIndex > allList.length - 1) {
        isQuestionScreen = false;
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (isQuestionScreen) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 249, 160, 63),
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
        body: Stack(
          children: [
            Container(
              decoration:const BoxDecoration(
                image:DecorationImage(
                  image:AssetImage(
                    "assets/images/theme.jpg",
                  ),
                ),
              ),
            ),
            Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                
                const SizedBox(
                  height: 20,
                  width:500,
                ),
                Container(
                  width: 400,
                  alignment: Alignment.center,
                  child: Text("Questions:${questionIndex+1}/${allList.length}",
                      style:const TextStyle(
                        fontSize:20,
                        fontWeight: FontWeight.w600,
                        
                      ),
                      textAlign: TextAlign.center,
                      
                    ),
                ),

                const SizedBox(
                  height: 20,
                ),
                // Text("Questions:${allList[questionIndex].questions}",
                //   style:const TextStyle(
                //     fontSize:18,
                //   ),
                // ),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: "${allList[questionIndex].questions}",
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 19,
                        )
                        
                      ),
                    ],
                  ),
                ),
            
                const SizedBox(
                  height:20,
                ),
            
                //=================================Button 1============================
                SizedBox(
            
                  height: 50,
                  width: 300,
                  child: ElevatedButton(
                    
                    onPressed: () {
                      if (selectedIndex == -1) {
                        selectedIndex = 0;
                      }
                      setState(() {});
                    },
                    style: ButtonStyle(
                      backgroundColor: checkAnswer(0),
                    ),
                    
                    
                    child: Text("A. ${allList[questionIndex].options[0]}",
                    style:const TextStyle(
                      color:Color.fromARGB(255, 249, 160, 63),
                      fontWeight:FontWeight.bold,
                      fontSize:15,
                    ),),
                  ),
                ),
                const SizedBox(
                  height:20,
                ),
            
                //=================================Button 2============================
                SizedBox(
            
                  height: 50,
                  width: 300,
                  child: ElevatedButton(
                    
                    
                    onPressed: () {
                      if (selectedIndex == -1) {
                        selectedIndex = 1;
                      }
                      setState(() {});
                    },
                    style: ButtonStyle(
                      backgroundColor: checkAnswer(1),
                    ),
                    child: Text("B. ${allList[questionIndex].options[1]}",
                    style:const TextStyle(
                      color:Color.fromARGB(255, 249, 160, 63),
                      fontWeight:FontWeight.bold,
                      fontSize:15,
                    ),
                    ),
                  ),
                ),
            
                const SizedBox(
                  height:20,
                ),
                //=================================Button 3============================
                SizedBox(
            
                  height: 50,
                  width: 300,
                  child: ElevatedButton(
                    
                    onPressed: () {
                      if (selectedIndex == -1) {
                        selectedIndex = 2;
                      }
                      setState(() {});
                    },
                    style: ButtonStyle(
                      backgroundColor: checkAnswer(2),
                    ),
                    child: Text("C. ${allList[questionIndex].options[2]}",
                    style:const TextStyle(
                      color:Color.fromARGB(255, 249, 160, 63),
                      fontWeight:FontWeight.bold,
                      fontSize:15,
                    ),
                    ),
                  ),
                ),
            
                const SizedBox(
                  height:20,
                ),
            
                //=================================Button 4============================
                SizedBox(
            
                  height: 50,
                  width: 300,
                  child: ElevatedButton(
                    
                    onPressed: () {
                      if (selectedIndex == -1) {
                        selectedIndex = 3;
                      }
                      setState(() {});
                    },
                    style: ButtonStyle(
                      backgroundColor: checkAnswer(3),
                    ),
                    child: Text("D. ${allList[questionIndex].options[3]}",
                    style:const TextStyle(
                      color:Color.fromARGB(255, 249, 160, 63),
                      fontWeight:FontWeight.bold,
                      fontSize:15,
                    ),
                    ),
                  ),
                ),
            
                const SizedBox(
                  height:20,
                ),
            
              ],
            ),
          ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: const Color.fromARGB(255,249, 160, 63),
          onPressed: () {
            pageNavigation();
          },
          child: const Icon(Icons.navigate_next,),
        ),
      );
    } else {
      return Scaffold(
        body: Stack(
          children: [
            Container(
              decoration: const BoxDecoration(
                image:DecorationImage(
                  image:AssetImage(
                    "assets/images/theme.jpg",
                  ),
                  fit:BoxFit.cover,
                ),
              ),
            ),

            Center(
              child:Column(
                
                children:[
                Align(
                  alignment: Alignment.center,
                  child:Image.asset(
                    "assets/images/achievement.png",
                    height:500,
                    width:400,
                  ), 
                ),


                const Text(
                  "Congratulations!!!!",
                  style:TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),

               

                Text(
                  "Score: $score/${allList.length}",
                  style:const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),

                const SizedBox(
                  height:20,
                ),

                SizedBox(
                width: 300,
                height: 50,
                child: ElevatedButton(

                  style:  ButtonStyle(
                    shape: MaterialStatePropertyAll(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    backgroundColor: MaterialStatePropertyAll(
                      Colors.deepPurple.shade400,
                    ),
                  ),
                  
                  //  style: ElevatedButton.styleFrom(
                  //     backgroundColor: Colors.white,
                  //  ),
                  // 
                  // 
                  onPressed: () {
                    Navigator.push(context,
                      MaterialPageRoute(
                        builder: (context) => const QuizApp(),
                      ),
                    );
                  },
                  child: const Text(
                    "Play Again",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                ),
                ),
                ],
              ),
              
            ),
            
          ],
        ),
      );
    }
  }
}