import 'package:flutter/material.dart';
//import 'package:listassignment/normallist.dart';
//import 'package:listassignment/dynamic_list.dart';
import 'package:listassignment/listbuilder.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner:false,
      home: ListBuilder(),
    );
  }
}
