
import 'package:flutter/material.dart';

class ListBuilder extends StatefulWidget{
  const ListBuilder({super.key});

  @override
  State<ListBuilder> createState()=> _ListBuilderState();
}

class _ListBuilderState extends State<ListBuilder>{

  int counter = 0;
  List<int> list = List.empty(growable: true);
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home:Scaffold(
        appBar:AppBar(
          
          title:const Text("List Builder"),
          centerTitle:true,
        ),
        body:ListView.builder(
          itemCount:list.length,
          itemBuilder:(context,index){
            return Container(
              margin:const EdgeInsets.all(10),
              width:600,
              height:30,
              color:Colors.amberAccent,
              alignment:Alignment.center,
              child:Text(
                "Container${list[index]}",
              ),  
            );
          }

        ),

        floatingActionButton:FloatingActionButton(
          onPressed:(){
            setState((){
              if(counter < 4){
                counter++;
                list.add(counter);
              }
            });
          },
          child:const Icon(Icons.add),
        ),
      ),
    );
  }
}