import 'package:flutter/material.dart';

class NormalList extends StatefulWidget{
  const NormalList({super.key});

  @override
  State<NormalList> createState()=> _NormalListState();

}

class _NormalListState extends State<NormalList>{

  
  @override
  Widget build(BuildContext context){

    return MaterialApp(
      home:Scaffold(
        appBar:AppBar(
          title:const Text("NormalList "),
          centerTitle:true,
        ),

        body:ListView(
          children:[
            Container(
              margin:const EdgeInsets.all(10),
              child:Image.network(
                "https://media.istockphoto.com/id/942152278/photo/gadisar-lake-at-jaisalmer-rajasthan-at-sunrise-with-ancient-temples-and-archaeological-ruins.webp?b=1&s=612x612&w=0&k=20&c=vfdc2W9UJWQguU1XHCCO5E8rZARh7FrxJIgiY07QDWs=",
              ),
            ),
            Container(
              margin:const EdgeInsets.all(10),
              child:Image.network(
                "https://media.istockphoto.com/id/1401576772/photo/environment-day-and-earth-day-concept.webp?s=2048x2048&w=is&k=20&c=ak2sXuoNJRmtjjK2XY5q0sM6G1yxgQbI62e9E4URGl4=",
              ),
            ),
            Container(
              margin:const EdgeInsets.all(10),
              child:Image.network(
                "https://cdn.pixabay.com/photo/2016/03/04/19/36/beach-1236581_1280.jpg",
              ),
            ),
          ],
        ),

      ),
    );
  }
}

