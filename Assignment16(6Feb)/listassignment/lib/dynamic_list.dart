import 'package:flutter/material.dart';

class DynamicList extends StatefulWidget{
  const DynamicList({super.key});

  @override
  State<DynamicList> createState()=> _DynamicListState();
}


class _DynamicListState extends State<DynamicList>{


  List<String> imageList = [
   
    "https://media.istockphoto.com/id/942152278/photo/gadisar-lake-at-jaisalmer-rajasthan-at-sunrise-with-ancient-temples-and-archaeological-ruins.webp?b=1&s=612x612&w=0&k=20&c=vfdc2W9UJWQguU1XHCCO5E8rZARh7FrxJIgiY07QDWs=",
    "https://media.istockphoto.com/id/1401576772/photo/environment-day-and-earth-day-concept.webp?s=2048x2048&w=is&k=20&c=ak2sXuoNJRmtjjK2XY5q0sM6G1yxgQbI62e9E4URGl4=",
    "https://cdn.pixabay.com/photo/2016/03/04/19/36/beach-1236581_1280.jpg",
    
  ];
  @override
  Widget build(BuildContext context){
    
    return MaterialApp(
      home:Scaffold(
        appBar:AppBar(
          title:const Text("Dynamic List"),
          centerTitle:true,
        ),

        body:ListView.builder(
          
          itemCount:imageList.length,
          itemBuilder:(context,index){
            return Container(
              margin:const EdgeInsets.all(10),
              child:Image.network(
                imageList[index],
              ),
            );
          },

        ),
      ),
    );
  }
}