import 'package:flutter/material.dart';

class Portfolio extends StatefulWidget{
  const Portfolio({super.key});

  @override
  State<Portfolio> createState()=> _PortfolioState();
}

class _PortfolioState extends State<Portfolio>{

  int _count = -1;
  
  
  @override
  Widget build(BuildContext context){
    
    return Scaffold(
    
        appBar:AppBar(
          title:const Text("Portfolio",
          style:TextStyle(
            fontStyle:FontStyle.italic,
          ),
          ),
          backgroundColor: Color.fromARGB(183, 249, 159, 4),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: (){
            setState((){
                _count = _count + 1;
            });
          },
          child:const Text("Click"),
        ),


        body: SingleChildScrollView(
          child: Container(
            
            color:const Color.fromARGB(255, 242, 205, 205),
            padding:const EdgeInsets.all(8.0),
            width:double.infinity,
            //height:double.infinity,
            child: Column(
              
              mainAxisAlignment:MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children:[
                  
                const SizedBox(
                  height:20,
                  width:20,
                ),
          
                (_count >= 0)
                  ? Container(
                    padding:const EdgeInsets.all(8.0),
                    height:100,
                    child: const Text(
                        "Name: Mohit Meghraj Bawankar",
                        style:TextStyle(
                          fontSize:20,
                          fontStyle:FontStyle.italic,
                        ),
                        ),
                  )
                  : Container(
                    // height:100,
                  ),
          
                // const SizedBox(
                //   height:20,
                // ),
          
                (_count >= 1)
                  ? Container(
                    height:250,
                    child: Image.asset(
                      "assets/images/profilephoto.jpg",
                      width:300,
                      height:300,
                      ),
                  )
                  : Container(
                    height:100,
                  ),
          
                const SizedBox(
                  height:40,
                 
                ),
          
                (_count >= 2)
                  ? Container(

                    padding:const EdgeInsets.all(8.0),
                    height:100,
                    child: const Text("College : Zeal College Of Engineering And Research, Pune",
                                style:TextStyle(
                          
                                  fontSize:20,
                                  fontStyle:FontStyle.italic,
                        ),
                      ),
                  )
                  : Container(
                    height:100,
                  ),
          
                // const SizedBox(
                //   height:20,
                //   width:20,
                // ),
          
                (_count >= 3)
                  ? Container(

                    //padding:const EdgeInsets.all(8.0),
                    height:100,
                    child: Image.asset(
                      "assets/images/clglogo.png",
                      width:300,
                      height:300,
                    ),
                  )
                  : Container(
                    height:100,
                  ),
                
                const SizedBox(
                  height:20,
                  width:20,
                ),
          
                (_count >= 4)
                  ? Container(

                    padding: const EdgeInsets.all(8.0),
                    height:100,
                    child: const Text("Dream Company : Adobe",
                                  style:TextStyle(
                          
                                    fontSize:20,
                                    fontStyle:FontStyle.italic,
                        ),
                      ),
                  )
                  : Container(
                    height:100,
                  ),
          
                // const SizedBox(
                //   height:20,
                //   width:20,
                // ),
          
                (_count >= 5)
                  ? Container(

                    padding:const EdgeInsets.all(8.0),
                    height:80,
                    child: Image.asset(
                      "assets/images/adobelogo.png",
                      width:300,
                      height:200,
                    ),
                  )
                  : Container(
                    height:100,
                  ),
              ],
            ),
          ),
        ),

    );
  }
}