
import 'package:flutter/material.dart';

class IndianFlag extends StatelessWidget{
  const IndianFlag({super.key});

  @override
  Widget build(BuildContext context){

      return Scaffold(
        backgroundColor: Color.fromARGB(121, 194, 207, 243),
        appBar:AppBar(
          backgroundColor: Colors.orange,
          title:const Text(
            "Happy Republic Day!!!",
            style:TextStyle(
              fontSize:20,
            ),
          ),
          centerTitle:true,
        ),

        body: Container(

          // decoration: BoxDecoration(
          //   gradient:
          // ),
          width:double.infinity,
          height:400,

  
              child:Column(
                mainAxisAlignment:MainAxisAlignment.center,
                //mainAxisSize:MainAxisSize.max,
                //crossAxisAlignment:CrossAxisAlignment.center,

                children:[

                    Container(
                      height:66.66,
                      width:300,
                      color:Colors.orange,
                    ),
                    
                    Container(
                      height:66.66,
                      width:300,
                      color:Colors.white,

                      child:Image.asset(
                        "assets/images/ashokchakra1.jpg",
                      ),
                    ),

                    Container(
                      height:66.66,
                      width:300,
                      color:Colors.green,
                    ),

                ],
              ),
            
          ),
        
      );
  }
}